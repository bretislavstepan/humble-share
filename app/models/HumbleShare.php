<?php

namespace App\Models;


use Nette\Application\Responses\FileResponse;
use Nette\Database\Context;
use Nette\Http\FileUpload;
use Nette\InvalidStateException;


class HumbleShare
{

    /**
     * @var string
     */
    private $storageDir;

    /**
     * @var Context
     */
    private $database;

    public function __construct($storageDir, Context $database)
    {
        $this->storageDir = $storageDir;
        $this->database = $database;
    }

    public function save(FileUpload $file)
    {
        $uuid = uniqid();

        if (!$file->isOk()) {
            throw new InvalidStateException();
        }

        $file->move($this->storageDir . '/' . $uuid);
        $this->database->table('files')->insert([
            'filename' => $file->name,
            'uuid' => $uuid,
        ]);

        return $uuid;
    }

    private function fetchFilename(string $uuid)
    {
        $filename = $this->database->table('files')->get($uuid);

        if (!$filename) {
            throw new InvalidStateException();
        }

        return $filename->filename;
    }

    /**
     * @return FileResponse
     */
    public function load($uiid)
    {
        $pathToFile = sprintf('%s/%s', $this->storageDir, $uuid);
        $name = $this->fetchFilename($uuid);
        return new FileResponse($pathToFile, $name);
    }

}
