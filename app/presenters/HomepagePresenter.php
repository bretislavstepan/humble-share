<?php

namespace App\Presenters;

use App\Models\HumbleShare;
use Nette;


class HomepagePresenter extends Nette\Application\UI\Presenter
{

    /**
     * @var HumbleShare
     */
    private $humbleShare;

    /**
     * @var string
     */
    private $uuid;

    public function __construct(HumbleShare $share)
    {
        $this->humbleShare = $share;
    }

    public function renderDefault()
    {
        $this->template->uuid = $this->uuid;
    }


    public function createComponentForm()
    {
        $form = new Nette\Application\UI\Form();

        $form->addUpload('file', 'File:');
        $form->addSubmit('share', 'Share');

        $form->onSuccess[] = [$this, 'save'];

        return $form;
    }

    public function save(Nette\Application\UI\Form $form, $values)
    {
        $fileUpload = $values->file;
        $this->uuid = $this->humbleShare->save($fileUpload);
    }

    public function actionDownload($uuid)
    {
        $this->sendResponse($this->humbleShare->load($uuid));
    }

}
